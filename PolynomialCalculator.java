// Name: Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA2
// Fall 2016
import java.util.Scanner;
import java.util.ArrayList;

/*
 * An interactive polynomial calculator that allow the user to create, evaluate, add and finally print 
 * the polynomials in the following form: 4-term polynomial: 3.0x^5 + -x^2 + x + -7.9
 */

public class PolynomialCalculator {
    
    private static final int NOT_VALID = -1;
    private static final int ARRAY_SIZE = 10;
    
    public static void main(String[] args)
    {   
        boolean quit = false;                           //Used to determine the end of the program with command quit.
        Polynomial[] myPolyArray = new Polynomial[ARRAY_SIZE];  //Array of 10 Polynomials.
        initPoly(myPolyArray);                          //Initialize array of polynomials with zero-term polynomials.
        Scanner lineScanner  = new Scanner(System.in);   
        ArrayList<Double> myParam = new ArrayList<Double>(); //Saves parameters of commands E.g add 1 2 3 (parameters are 1,2,3). 
        
        System.out.print("For more information about application usage.Use command help cmd> help\ncmd>");
        while(quit == false && lineScanner.hasNextLine() )
        {
            String line1 = lineScanner.nextLine();
            if(line1.isEmpty())
            {
                line1 = "No_Command";
            }
            Scanner scan2 = new Scanner(line1);
            String command = scan2.next();
        
            while(scan2.hasNextDouble())
            {
               myParam.add(scan2.nextDouble()); 
            }
            
            switch (isValidMethod(command)) //Determine the command to run.
            {
                case -1: System.out.println("ERROR: Illegal command.  Type 'help' for command options."); break;
                case 0:  doCreate(myParam,myPolyArray); break;
                case 1:  doPrintPoly(myParam,myPolyArray); break;
                case 2:  doAdd(myParam,myPolyArray); break;
                case 3:  doEvalPoly(myParam,myPolyArray); break;
                case 4:  doHelp(); break;
                case 5:  quit = true; break;
            }
            myParam.clear();
            if(quit == false)
            {
                System.out.print("cmd>");
            }
            else
            {
                lineScanner.close();
                scan2.close();
            }
        }
    }  
    
    /**
     Initialize the Arrays of Polynomials to zero-term polynomials.
     */
    private static void initPoly (Polynomial[] polyArray)
    {
        for (int i =0; i <polyArray.length; i++)
        {
            polyArray[i] = new Polynomial();
        }
    }
    
    /**
     Determine if the name of the method is a valid method. If it is a valid method return the number of method.
     */
    private static int isValidMethod (String command)
    {
        String[] commandsArray = {"create", "print", "add", "eval", "help","quit" };
        for(int i = 0; i<commandsArray.length; i ++)
        {
            if(command.equalsIgnoreCase(commandsArray[i]))
            {
               return i;
            }
        }  
        return NOT_VALID ;
    }
    
    /**
    Determine if the number of index to be created, evaluated, added, printed is a correct index and returns true.
    If it is not a correct index return false;
    */
    private static boolean isValidIndex(ArrayList<Double> myParam, Polynomial[] myPolyArray)
    {
        for (int i =0; i <myParam.size(); i ++)
        {
            if(myParam.get(i) >= myPolyArray.length || myParam.get(i) < 0 )
            {
                 return false;
            }
        }
        return true;
    }
    
    /**
    Determine if missing last exponent in create method and delete the last value entered.
    */
    private static void isMissingExponent(ArrayList<Double> termsArr)
    {
        if(termsArr.size() % 2 != 0)
        {
            termsArr.remove(termsArr.size()-1);
            System.out.println("WARNING: Missing the last exponent, removing last entered number.");
        }
    }
    
    /**
    Determine if the user introduced negative exponents in create method. Return true if negative exponents entered.
    */
    private static void isNegativeExponent(ArrayList<Double> termsArr)
    {
        for(int i =0; i <termsArr.size(); i ++)
        {
            if(i%2 != 0)
            {
                if(termsArr.get(i) < 0)
                {
                termsArr.set(i, Math.abs(termsArr.get(i)));
                System.out.println("WARNING: Negative exponents detected at position " + i +", converting to positive exponent.");
                }
            }
        }
    }
      
    /**
    Assign a value to a polynomial. 
    */
    private static void doCreate(ArrayList<Double> myParam, Polynomial[] myPolyArray)
    {
        Scanner doScan1 = new Scanner(System.in);
        ArrayList<Double> termsArr = new ArrayList<Double>();
        if(myParam.size() != 1)
        {
            System.out.println("ERROR: Illegal command.  Type 'help' for command options.");
        }
        else
        {
            if(!isValidIndex(myParam,myPolyArray))
            {
            System.out.println("ERROR: illegal index for a poly.  must be between 0 and 9, inclusive");
            }
            else
            {
                System.out.println("Enter a space-separated sequence of coeff-power pairs terminated by <nl>");
                String line = doScan1.nextLine();
                Scanner doScan2 = new Scanner(line);
                while(doScan2.hasNextDouble())
                {
                    termsArr.add(doScan2.nextDouble());
                }
                if(termsArr.size()>1)
                {
                    isMissingExponent(termsArr);
                    isNegativeExponent(termsArr);
           
                    double value = myParam.get(0); 
                    myPolyArray[(int)value] = new Polynomial();
                    for(int i =0; i <termsArr.size(); i=i+2)
                    {
                        double coeff=termsArr.get(i); 
                        double expon=termsArr.get(i+1);
                        myPolyArray[(int)value] =  myPolyArray[(int)value].add(new Polynomial(new Term(coeff,(int)expon)));
                    }
                }
                else
                {
                    System.out.println("WARNING: NO TERMS GIVEN"); 
                }
            }
        }
    }
    
    /**
    Return a String version of the polynomial with the 
    following format, shown by example:
    zero poly:   "0.0"
    1-term poly: "3.2x^2"
    4-term poly: "3.0x^5 + -x^2 + x + -7.9"

    Polynomial is in a simplified form (only one term for any exponent),
    with no zero-coefficient terms, and terms are shown in
    decreasing order by exponent.
     */
    private static void doPrintPoly(ArrayList<Double> myParam, Polynomial[] myPolyArray)
    {
        if(myParam.size() != 1)
        {
            System.out.println("ERROR: Illegal command.  Type 'help' for command options.");
        }
        else
        {
            if(!isValidIndex(myParam,myPolyArray))
            {
            System.out.println("ERROR: illegal index for a poly.  must be between 0 and 9, inclusive");
            }
            else
            {
                double value = myParam.get(0); 
                System.out.println(myPolyArray[(int)value].toFormattedString());
            }
        }
    }
    
    /**
    Sum polynomials b and c and assign the value to polynomial a. 
    */
    private static void doAdd (ArrayList<Double> myParam, Polynomial[] myPolyArray)
    {
        if(myParam.size() != 3)
        {
            System.out.println("ERROR: Illegal command.  Type 'help' for command options.");
        }
        else
        {    
            if(!isValidIndex(myParam,myPolyArray))
            {
                System.out.println("ERROR: illegal index for a poly.  must be between 0 and 9, inclusive");  
            }
            else
            {
                double param0 = myParam.get(0);
                double param1 = myParam.get(1);
                double param2 = myParam.get(2);
                myPolyArray[(int)param0] = myPolyArray[(int)param1].add(myPolyArray[(int)param2]); 
            }
        } 
    }
    
    /**
    Returns the value of the polynomial at a given value of x.
    */
    private static void doEvalPoly (ArrayList<Double> myParam, Polynomial[] myPolyArray)
    {
        Scanner evalScan = new Scanner(System.in);
        if(myParam.size() != 1)
        {
            System.out.println("ERROR: Illegal command.  Type 'help' for command options.");
        }
        else
        {
            if(!isValidIndex(myParam,myPolyArray))
            {
            System.out.println("ERROR: illegal index for a poly.  must be between 0 and 9, inclusive");
            }
            else
            {
              System.out.print("Enter a floating point value for x: ");
              double xValue = evalScan.nextDouble();
              double value = myParam.get(0);
              System.out.println(myPolyArray[(int)value].eval(xValue));
            }
        } 
    }
    /**
    Shows the user the application manual. 
    */
    
    private static void doHelp()
    {
        System.out.println("List of Supported Commands: \n"
                + "-----Create x----- \nParameters int x: "
                + "Integer between 0 to 9 inclusive which is the polynomial to be created.\n"
                + "After pressing enter the following message will appear: \n"
                + "Enter a space-separated sequence of coeff-power pairs terminated by <nl> \n"
                + "E.g. -1 100 -4 2 -24 0 \n \n"
                + "-----Print x-----\nParameters int x: "
                + "Integer between 0 to 9 inclusive which is the polynomial to be printed.\n"
                + "Print the specified polynomial with the following format, shown by example:\n"
                + "zero poly: 0.0; 1-term poly: 3.2x^2; 4-term poly: 3.0x^5 + -x^2 + x + -7.9\n"
                + "Polynomial is in a simplified form,  with no zero-coefficient terms and decreasing order\n\n"
                + "-----Add x y z ------\npoly x = poly y + poly z\n"
                + "Parameters int x, y, z: Integers between 0 to 9 inclusive.\n"
                + "Polynomial x will be equal to polynomial y + polynomial z \n\n"
                + "-----Eval x----- \nParameters int x: "
                + "Integer between 0 to 9 inclusive which is the polynomial to be evaluated.\n"
                + "After pressing enter the following message will appear: \n"
                + "Enter a floating point value for x: E.g 17 \n\n"
                + "-----Help-----\n"
                + "Shows information about calculator commands and usage\n\n"
                + "-----Quit-----\n"
                + "Exit the application\n\n");
    }
}
