// Name: Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA2
// Fall 2016


import java.util.ArrayList;

/**
   A polynomial. Polynomials can be added together, evaluated, and
   converted to a string form for printing.
*/
public class Polynomial {

    /**
       Creates the 0 polynomial
    */
    public Polynomial() {
        this.polyTerms = new ArrayList<Term>(); 
        assert this.isValidPolynomial(); 
    }


    /**
       Creates polynomial with single term given
     */
    public Polynomial(Term term) {
        this.polyTerms = new ArrayList<Term>();
        if(term.getCoeff() != 0)     
        {
            this.polyTerms.add(term);   
        }
        assert this.isValidPolynomial(); 
    }
    /**
       Returns the Polynomial that is the sum of this polynomial and b
       (neither poly is modified)
     */
    public Polynomial add(Polynomial b) {
        Polynomial sum = new Polynomial();
        int aFirst = 0 , bFirst = 0;
        
        while(aFirst < this.polyTerms.size() && bFirst<b.polyTerms.size())
        {
            if(this.polyTerms.get(aFirst).getExpon() > b.polyTerms.get(bFirst).getExpon())
            {
                sum.polyTerms.add(this.polyTerms.get(aFirst));
                aFirst++;
            }
            else if(b.polyTerms.get(bFirst).getExpon() > this.polyTerms.get(aFirst).getExpon())
            {
                sum.polyTerms.add(b.polyTerms.get(bFirst));
                bFirst++;
            }
            else
            {   
                double termCoefficient = b.polyTerms.get(bFirst).getCoeff() + this.polyTerms.get(aFirst).getCoeff();
                if(termCoefficient != 0)
                {
                    int termExponent = this.polyTerms.get(aFirst).getExpon();
                    sum.polyTerms.add(new Term(termCoefficient,termExponent));
                }
                aFirst++;
                bFirst++;
            }
        }
        while (aFirst < this.polyTerms.size() )
        {
            sum.polyTerms.add(this.polyTerms.get(aFirst));
            aFirst++;
        }      
        while (bFirst < b.polyTerms.size() )
        {
            sum.polyTerms.add(b.polyTerms.get(bFirst));
            bFirst++;
        }
        assert this.isValidPolynomial();
        assert b.isValidPolynomial();
        assert sum.isValidPolynomial();
        return sum;
    }   
        

    /**
       Returns the value of the poly at a given value of x.
     */
    public double eval(double x) {
        double evalResult = 0;
        
        for (int i =0; i< this.polyTerms.size(); i++)
        {
            evalResult+= this.polyTerms.get(i).getCoeff() * Math.pow(x,this.polyTerms.get(i).getExpon());
        }
        assert this.isValidPolynomial();
        return evalResult;
        
    }


    /**
       Return a String version of the polynomial with the 
       following format, shown by example:
       zero poly:   "0.0"
       1-term poly: "3.2x^2"
       4-term poly: "3.0x^5 + -x^2 + x + -7.9"

       Polynomial is in a simplified form (only one term for any exponent),
       with no zero-coefficient terms, and terms are shown in
       decreasing order by exponent.
    */
    public String toFormattedString() {
        String resultString = ""; 
        if(polyTerms.size() ==0)
        {
            resultString = "0.0";
        }
        else
        {
            for (int i=0; i <polyTerms.size(); i++)
            {
                if(i>0)
                {
                    resultString+= " + ";
                }
                if(Math.abs(this.polyTerms.get(i).getCoeff()) != 1 || this.polyTerms.get(i).getExpon() ==0)
                {
                    resultString+= this.polyTerms.get(i).getCoeff();
                }            
                if(this.polyTerms.get(i).getCoeff() != -1 && this.polyTerms.get(i).getExpon() !=0)
                {
                    resultString+= "x";
                }
                else if(this.polyTerms.get(i).getCoeff() == -1  && this.polyTerms.get(i).getExpon() !=0)
                {
                    resultString+= "-x";
                }
                if(this.polyTerms.get(i).getExpon() > 1 )
                {
                    resultString+= "^" + this.polyTerms.get(i).getExpon();
                }
            }
        }
        assert this.isValidPolynomial();
        return resultString;
    }
    
    /**
    Returns a String version of the Polynomial in the format:
    Term[coeff=" + coeff + ",expon=" + expon + "] for each term.
   */
    public String polynomialToString()
    {
        String polynomialString = "";
        for(int i = 0; i <this.polyTerms.size(); i ++)
        {
            polynomialString += this.polyTerms.get(i).toString();
        }
        assert this.isValidPolynomial();
        return polynomialString;
    }

    // **************************************************************
    //  PRIVATE METHOD(S)

    /**
       Returns true iff the poly data is in a valid state.
    */  
    private boolean isValidPolynomial() {
        boolean testAlphaOrder = isAlphaOrder(); 
        boolean testNonZeroExp = hasNonZeroExponentsOrCoefficients();
        boolean testNonRepeatedTerms = hasNonRepeatedTerms();
        
        if(testAlphaOrder == true && testNonZeroExp == true && testNonRepeatedTerms == true)
        {
           return true; 
        }
        return false;
    }
    
    /**
     Returns true if the polynomial is in Greater to Lower Order
     */
    private boolean isAlphaOrder() {
        if(this.polyTerms.size()>0)
        {    
            int firstExponent = this.polyTerms.get(0).getExpon();
            for(int i =1; i <this.polyTerms.size(); i++)
            {   
                if(this.polyTerms.get(i).getExpon() < firstExponent) //Check the order of the exponents
                {
                    firstExponent = this.polyTerms.get(i).getExpon();
                }
                else
                {   
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
    Returns true if the polynomial does not have zero exponents or Coefficients.
    */
    private boolean hasNonZeroExponentsOrCoefficients() {
        if(this.polyTerms.size()>0)
        {    
            for(int i =0; i <this.polyTerms.size(); i++)
            {   
                if(this.polyTerms.get(i).getExpon() < 0 || this.polyTerms.get(i).getCoeff() == 0 ) //Check exp or coeff
                {  
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
    Returns true if the polynomial does not have repeated exponents.
    */
    private boolean hasNonRepeatedTerms() {
        if(this.polyTerms.size()>0)
            {    
                for(int i =0; i <this.polyTerms.size(); i++)
                {       
                    for(int j = 0; j<this.polyTerms.size();j++)
                    {
                        if(i != j)
                        {
                            if(this.polyTerms.get(i).getExpon() == this.polyTerms.get(j).getExpon()) 
                            {  
                                return false;
                            }
                        }
                    }   
                }
            }
        return true;
    }
    
    // **************************************************************
    //  PRIVATE INSTANCE VARIABLE(S)
    ArrayList<Term> polyTerms;
    
    //LIST OF REPRESENTATION INVARIANTS
    //  1. For a non-zero poly all the terms must be in decreasing order by exponent. 
    //  2. A non-zero poly can not contain two terms with the same exponent.
    //  3. A non-zero poly can not contain a term with coefficient = 0;
    //  4. Exponent of a non-zero poly has to be >=0. 
}