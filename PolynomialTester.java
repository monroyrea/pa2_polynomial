// Name: Alejandro Monroy Reyes
// USC loginid: monroyre
// CS 455 PA2
// Fall 2016

public class PolynomialTester {
    public static void main(String[] args)
    {
        System.out.println("Creating P0 ");
        Polynomial p0 = new Polynomial(new Term(1, 100)).add(new Polynomial(new Term(3, 2)));
        System.out.println(p0.toFormattedString());
        System.out.println(p0.polynomialToString());
        System.out.println("");
        
        
        System.out.println("Creating P1 ");
        Polynomial p1 = new Polynomial(new Term(1, 2)).add(new Polynomial(new Term(2, 1)).add(new Polynomial(new Term(1,0))));
        System.out.println(p1.toFormattedString());
        System.out.println(p1.polynomialToString());
        System.out.println("");
        
        System.out.println("Creating P2 ");
        Polynomial p2 = p0.add(p1);
        System.out.println(p2.toFormattedString());
        System.out.println(p2.polynomialToString());
        System.out.println("");
        
        System.out.println("Creating P3 ");
        Polynomial p3 = new Polynomial(new Term(20, 0)).add(new Polynomial(new Term(3, 3)));
        System.out.println(p3.toFormattedString());
        System.out.println(p3.polynomialToString());
        System.out.println("");
        
        System.out.println("Updating P2");
        p2 = p2.add(p3);
        System.out.println(p2.toFormattedString());
        System.out.println(p2.polynomialToString());
        System.out.println("");
        
        System.out.println(p3.eval(2.0));
        
        System.out.println("Creating P4");
        Polynomial p4 = new Polynomial(new Term(-1, 100)).add(new Polynomial(new Term(-4, 2)).add(new Polynomial(new Term(-24,0))));
        System.out.println(p4.toFormattedString());
        System.out.println(p4.polynomialToString());
        System.out.println("");
        
              
        System.out.println("Creating P5");
        Polynomial p5 = p4.add(p2);
        System.out.println(p5.toFormattedString());
        System.out.println(p5.polynomialToString());
        System.out.println("");
        
        System.out.println("Creating 1");
        p1 = new Polynomial(new Term(-3, 20)).add(new Polynomial(new Term(0, 1)).add(new Polynomial(new Term(52.3,0)).add(new Polynomial(new Term(5,10)).add(new Polynomial(new Term(12,0)).add(new Polynomial(new Term(-2,10)))))));
        System.out.println(p1.toFormattedString());
        System.out.println(p1.polynomialToString());
        System.out.println("");
        
        System.out.println("Creating 9");
        Polynomial p9 = new Polynomial();
        System.out.println(p9.toFormattedString());
        System.out.println(p9.polynomialToString());
        
        System.out.println("Creating 10");
        Polynomial p10 = new Polynomial(new Term(3,3));
        p10 = p10.add(p9);
        System.out.println(p10.toFormattedString());
        System.out.println(p10.polynomialToString());
        
        
    }
}
